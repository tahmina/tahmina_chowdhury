<?php
/*
Template Name: RSS 3 Column
*/

$rss = array(
    'top-storries' => 'http://feeds.bbci.co.uk/news/rss.xml',
    'business' => 'http://feeds.bbci.co.uk/news/business/rss.xml',
    'technology' => 'http://feeds.bbci.co.uk/news/technology/rss.xml'
);

$ress_resp = array();

foreach($rss as $key=>$r) 
{
    
 $bbc_feeds = file_get_contents($r); 

 $ress_resp[$key]= simplexml_load_string($bbc_feeds);
}

?>
<?php get_header(); ?>
			
			<div id="content" class="clearfix row">
			
				<div id="main" class="col col-lg-12 clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<header>
							
							<div class="page-header"><h1><?php the_title(); ?></h1></div>
						
						</header> <!-- end article header -->
					
						<section class="post_content">
							<?php the_content(); ?>
                                                    
                                                    <div col4 >
					
						</section> <!-- end article section -->
						
						<footer>
			
							<p class="clearfix"><?php the_tags('<span class="tags">' . __("Tags","wpbootstrap") . ': ', ', ', '</span>'); ?></p>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
					
					
					
					<?php endwhile; ?>	
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->

				<?php //get_sidebar(); // sidebar 1 ?>
                                <div id="content" class="clearfix row">
                                    <div class="col col-lg-4 clearfix top-story" >
                                       <h2>To Stories</h2>
                                       
                                       <?php
                                        $topStories = json_decode(json_encode($ress_resp['top-storries']), true);
                                        $items = $topStories['channel']['item'];
                                        
                                        $i = 0;
                                        foreach($items as $rss_item) 
                                        {
                                           echo "<div class='row'>";
                                           echo "<h3><a href='{$rss_item['link']}'>{$rss_item['title']}</a></h3>";
                                           echo "<p>{$rss_item['description']}</p>";
                                           echo "</div>";
                                           $i++;
                                           if($i == 5) 
                                               break;
                                        }
                                       ?>
                                       
                                    </div>
                                    <div class="col col-lg-4 clearfix business" >
                                        <h2>Business</h2>
                                        <?php
                                        $business = json_decode(json_encode($ress_resp['business']), true);
                                        $items = $business['channel']['item'];
                                        
                                        $i = 0;
                                        foreach($items as $rss_item) 
                                        {
                                           echo "<div class='row'>";
                                           echo "<h3><a href='{$rss_item['link']}'>{$rss_item['title']}</a></h3>";
                                           echo "<p>{$rss_item['description']}</p>";
                                           echo "</div>";
                                           $i++;
                                           if($i == 5) 
                                               break;
                                        }
                                       ?>
                                    </div>
                                    <div class="col col-lg-4 clearfix technology" >
                                        <h2>Technology</h2>
                                        <?php
                                        $technology = json_decode(json_encode($ress_resp['technology']), true);
                                        $items = $technology['channel']['item'];
                                        
                                        $i = 0;
                                        foreach($items as $rss_item) 
                                        {
                                           echo "<div class='row'>";
                                           echo "<h3><a href='{$rss_item['link']}'>{$rss_item['title']}</a></h3>";
                                           echo "<p>{$rss_item['description']}</p>";
                                           echo "</div>";
                                           $i++;
                                           if($i == 5) 
                                               break;
                                        }
                                       ?>
                                    </div>
                                </div>  
			</div> <!-- end #content -->

<?php get_footer(); ?>